#include<stdio.h>
int main()
{	
	int a[100],b,smaller,bigger,i;
	int loc_bigger,loc_smaller;
	printf("enter the number of elements to enter : \n");
	scanf("%d",&b);
	printf("enter the elements : \n");
	for(i=0;i<b;i++)
	{
		scanf("%d",&a[i]);
	}
	smaller = a[0];
	bigger = a[0];
	for(i=0;i<b;i++)
	{
		if(a[i] <= smaller)
		{
			smaller = a[i];
			loc_smaller = i;
		}
		if(a[i] >= bigger)
		{	
			bigger = a[i];
			loc_bigger = i;
		}
	}
	printf("before swapping : \n");
	for(i=0;i<b;i++)
	{
		printf("%d\n",a[i]);
	}
	printf("biggest number is %d and location is = %d : \n",bigger,loc_bigger);
	printf("Smallest number is %d and location is = %d : \n",smaller,loc_smaller);
	int temp;
	temp = a[loc_smaller];
	a[loc_smaller] = a[loc_bigger];
	a[loc_bigger] = temp;
	printf("after swapping : \n");
	for(i=0;i<b;i++)
	{
		printf("%d\n",a[i]);
	}
	return 0;
}
